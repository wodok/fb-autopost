# Post Automation Using Selenium, Pyautogui


## Requirements

- Selenium- **pip3 install selenium**
- Chrome Web Driver-**[ChromeDriver - WebDriver for Chrome](https://sites.google.com/chromium.org/driver/)**
- pyautogui - **pip3 install pyautogui** 


## Run
Rename ***secrets_example.json*** to ***secrets.json*** and fill in required information. Then:
> python3 fb_autopost.py "Caption" <file.mp4> <"https://yoursite.com/groups/3823042401">

## Disclaimer

The provided code is presented as an example for educational purposes only. It is not intended to be used in a live or production environment, as doing so may violate the terms and conditions of the services or platforms involved. We strongly advise against using this code for any practical applications and recommend that you consult the official documentation and adhere to the terms of service of the respective services or platforms. The use of this code in any unauthorized manner is solely at your own risk, and we disclaim any responsibility for its misuse.
