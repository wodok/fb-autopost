import sys
from selenium import webdriver
import time
from selenium.webdriver.common.by import By
import os
import logging
from fb_config import FB_autopost_config as config
import chromedriver_autoinstaller
import traceback

# Configure the logging
logging.basicConfig(
    level=config.LOGGING,  # Set the logging level to capture ERROR and above
    format='%(asctime)s [%(levelname)s]: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[
        logging.StreamHandler()
    ]
)

def upload(user, p, filename, caption, login_url, target_url, sign):
    #####################configurations#############################
    chromedriver_autoinstaller.install()
    option = webdriver.ChromeOptions()
    option.add_argument("--disable-infobars")
    # option.add_argument("start-maximized")
    option.add_argument("--disable-extensions")
    # Pass the argument 1 to allow and 2 to block
    option.add_experimental_option(
        "prefs",
        {
            "profile.default_content_setting_values.media_stream_mic": 1,
            "profile.default_content_setting_values.media_stream_camera": 1,
            "profile.default_content_setting_values.geolocation": 1,
            "profile.default_content_setting_values.notifications": 1,
        },
    )
    option.add_argument("user-data-dir=/home/jzak/prf-muni-recordings/selenium")

    # Open main window with URL A
    browser = webdriver.Chrome(options=option)

    logging.info("Executing...")
    status = True
    try:
        try:
            status &= login(browser, user, p, login_url)
        except:
            logging.exception('')
            logging.warning('Login failed, but continuing...')
        browser.get(target_url)
        time.sleep(15)
        status &= post(browser, filename, caption, sign)
        logging.info("SUCCESS" if status else "FAIL")
    except Exception as e:
        print(traceback.format_exc())
        status = False
        logging.exception('')
        logging.info("FAIL")
    return status
    #####################configurations#############################


def login(browser, user, p, login_url):
    # login
    browser.get(login_url)
    time.sleep(15)
    try:
        element = browser.find_element(
            By.XPATH, "//form[contains(@action, 'logout.php')]"
        )
        logging.info("Login SUCCESFUL")
        # pyautogui.click(x=400, y=400)
        return True
    except:
        logging.info("Needs loging, proceeding...")
    browser.execute_script(
        "document.getElementById('email').setAttribute('type','password')"
    )
    email = browser.find_element(By.ID, "email")
    password = browser.find_element(By.ID, "pass")
    email.send_keys(user)
    password.send_keys(p)
    try:
        login = browser.find_element(
            By.XPATH, "//button[@data-testid='royal-login-button']"
        )
    except:
        try:
            allowCookies = browser.find_element(
                By.XPATH, "//div[@aria-label='Allow all cookies' and @tabindex='0']"
            )
            allowCookies.click()
            time.sleep(5)
        except:
            logging.exception('')
        login = browser.find_element(
            By.XPATH, "//button[@id='loginbutton']"
        )
    login.submit()
    time.sleep(10)
    #pyautogui.click(x=400, y=400)
    try:
        element = browser.find_element(
            By.XPATH, "//form[contains(@action, 'logout.php')]"
        )
        logging.debug("Login SUCCESFUL")
        return True
    except:
        logging.exception('')
        logging.debug("Login unsuccesful")

    return False
    # login ends

def post(browser, filename, caption, sign):
    try:
        _ = browser.find_element(
            By.XPATH, "//a[contains(text(), 'Studenti pro studenty PrF 2021')]"
        )
        logging.info("Group loaded")
    except:
        logging.warning('Group not found')

    dialog_detected = False
    caption_detected = False
    upload_detected = False
    publishing_detected = False
    processing_detected = False

    try:
        media = browser.find_element(By.XPATH, "//span[contains(text(), 'Fotka/video')]")
        media.click()
        time.sleep(5)
    except:
        logging.exception('')
        logging.error("FB create post button not found")
        return False

    try:
        _ = browser.find_element(
            By.XPATH, f"//div[@role='dialog']"
        )
        dialog_detected = True
        time.sleep(5)
    except:
        dialog_detected = False
        logging.info("Dialog not detected, using backup strategy.")

    if (dialog_detected == True):
        try:
            textInput = browser.find_element(
                By.XPATH, f"//div[@role='dialog']//div[@role='textbox' and @aria-label='Napište něco...']"
            )
            textInput.send_keys(caption + "\n\n" + sign)
        except:
            logging.info("Caption not filed")

        try:
            _ = browser.find_element(
                By.XPATH, f"//div[@role='dialog']//*[contains(text(), '{sign}')]"
            )
            caption_detected = True
            logging.info("FB caption check success")
            time.sleep(5)
        except:
            caption_detected = False

        if (caption_detected == False):
            logging.warning("FB caption check unsuccesful")


    if len(filename) > 0:

        try:
            file_input = browser.find_element(
                By.XPATH, f"//div[@role='dialog']//input[@type='file' and @accept='image/*,image/heif,image/heic,video/*,video/mp4,video/x-m4v,video/x-matroska,.mkv']"
            )
            file_input.send_keys(os.path.abspath(filename))
            logging.info('File selected')
        except:
            logging.info('File not selected. Terminating.')
            return False

        status = 0
        while status < 100:
            try:
                progressbar = browser.find_element(
                    By.XPATH,
                    "//div[@role='progressbar' and @aria-valuemax='100' and @aria-valuemin='0' and @aria-valuenow]",
                )
                pb_status = progressbar.get_attribute("aria-valuenow")
                if (upload_detected == False):
                    upload_detected = True
                    logging.info("FB upload detected")
                logging.debug(f"Upload: {pb_status} %")
                status = float(pb_status)
                time.sleep(5)
            except:
                time.sleep(3)
                logging.debug(f"Upload [unavailable]: {status} %")
                status += 1

    if (upload_detected == False):
        logging.warning("FB upload check unsuccesful")

    if (dialog_detected == False):
        logging.info("Dialog not detected")

    browser.execute_script("window.scrollBy(0,100)")
    time.sleep(5)
    button = browser.find_element(By.XPATH, "//div[@aria-label='Zveřejnit']")
    button.click()
    publishing = 0
    while publishing < 100:
        try:
            _ = browser.find_element(
                By.XPATH, "//span[contains(text(), 'Zveřejňování')]"
            )
            publishing = 50
            if (publishing_detected == False):
                publishing_detected = True
                logging.info("FB publishing detected")
            logging.debug("Publishing running")
            time.sleep(1)
        except:
            time.sleep(2)
            logging.debug("Publishing waiting")
            publishing += 10

    if (publishing_detected == False):
        logging.warning("FB publishing check unsuccesful")

    try:
        _ = browser.find_element(
            By.XPATH, "//span[contains(text(), 'Zpracovávání videa')]"
        )
        logging.info("FB processing detected")
        processing_detected = True
    except:
        processing_detected = False

    if (processing_detected == False):
        logging.warning("FB processing check unsuccesful")

    return processing_detected and upload_detected


def fb_upload(caption, filename=None, target_url=None):
    upload(config.USERNAME, config.PASSWORD, filename, caption, config.LOGIN_URL, target_url if target_url is not None else config.POST_URL, config.SIGNATURE)


if __name__ == "__main__":
    fb_upload(sys.argv[1], sys.argv[2] if len(sys.argv)>2 else None, sys.argv[3] if len(sys.argv)>3 else None)